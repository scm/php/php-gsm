# GSM PDU Library for PHP v1.0

Library for handling SMS messages via GSM-modem with a PHP interface. This library is extracted from a very old PHP utility that handled an old SMS gateway at "Tornevall Networks". It has been built mostly for understanding the SMS data protocols and the way of handling long concatenated SMS (that part is however not finished yet).
