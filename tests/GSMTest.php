<?php

namespace TorneLIB;

require_once(__DIR__ . "/../source/gsmlib.php");

use PHPUnit\Framework\TestCase;
use TorneLIB\TorneLIB_PDU;

class GSMTest extends TestCase
{

    /**
     * @var GSM $GSM
     */
    private $GSM;
    private $DEVICE = "/dev/ttyUSB0";
    private $DEVICE_FAIL = "/dev/ttyZ0";
    private $MOBILE_SMS_CENTRAL = "+46708000708"; // Telenor
    private $MOBILE_DESTINATION = "+46707303844";
    private $MOBILE_SELF = "+46702747703";
    private $MOBILE_DESTINATION_MESSAGE = "This is a swedish message, called räksmörgås!";

    /** @var TorneLIB_PDU $PDU */
    private $PDU;

    function setUp()
    {
        $this->PDU = new TorneLIB_PDU();
        $this->GSM = new GSM($this->DEVICE);
        $this->GSM->connect();
    }

    function testDeviceConnectUSB()
    {
        $standAloneDevice = new GSM($this->DEVICE);
        $this->assertTrue($standAloneDevice->connect());
    }

    function testDeviceConnectFail()
    {
        $isConnected = false;
        try {
            $standAloneDevice = new GSM($this->DEVICE_FAIL);
            $isConnected = $standAloneDevice->connect();
        } catch (\Exception $e) {
        }
        $this->assertFalse($isConnected);
    }

    function testDeviceRegularATWait()
    {
        $dataStream = $this->GSM->AT("", true);
        $this->assertTrue(isset($dataStream[1]) && $dataStream[1] === "OK");
    }

    function testDeviceCops()
    {
        $copsString = $this->GSM->COPS();
        $this->assertStringStartsWith("+COPS:", $copsString);
    }

    function testDeviceCReg()
    {
        $this->assertStringStartsWith("+CREG:", $this->GSM->CREG());
    }

    function testDeviceCimi()
    {
        $this->assertTrue(strlen($this->GSM->CIMI()) > 8);
    }

    function testDeviceCsq()
    {
        $this->assertStringStartsWith("+CSQ:", $this->GSM->CSQ());
    }

    function testDeviceCscs()
    {
        $this->assertStringStartsWith("+CSCS:", $this->GSM->CSCS());
    }

    function testSetDeviceText()
    {
        $this->assertTrue($this->GSM->setMode(MODEM_MODES::MODE_TEXT));
    }

    function testSetDevicePdu()
    {
        $this->assertTrue($this->GSM->setMode(MODEM_MODES::MODE_PDU));
    }

    function testSendSmsText($overrideMessage = '', $overrideReceiver = '')
    {
        $this->GSM->setMode(MODEM_MODES::MODE_TEXT);
        if (empty($overrideMessage)) {
            $this->assertTrue($this->GSM->sendSms($this->MOBILE_DESTINATION, $this->MOBILE_DESTINATION_MESSAGE));
        } else {
            return $this->GSM->sendSms($overrideReceiver, $overrideMessage);
        }
    }

    function testPduMessage($overrideMessage = '', $overrideReceiver = '')
    {
        $this->GSM->setMode(MODEM_MODES::MODE_PDU);
        $this->PDU->setPduSmsMessage($this->MOBILE_DESTINATION_MESSAGE);
        if (!empty($overrideMessage)) {
            $this->PDU->setPduSmsMessage($overrideMessage);
        }
        $this->PDU->setCentral($this->MOBILE_SMS_CENTRAL);
        $PDU_STRING = $this->PDU->getPduMessage($this->MOBILE_DESTINATION);
        if (!empty($overrideReceiver)) {
            $PDU_STRING = $this->PDU->getPduMessage($overrideReceiver);
        }
        $RES = $this->GSM->sendSms($PDU_STRING, $this->PDU->getPduSendLength());
        $this->assertStringStartsWith("+CMGS:", isset($RES[2]) ? $RES[2] : "");
        if (!empty($overrideMessage)) {
            return true;
        }
    }

    function testPduClass0Message()
    {
        $this->GSM->setMode(MODEM_MODES::MODE_PDU);
        $this->PDU->setPduSmsMessage($this->MOBILE_DESTINATION_MESSAGE);
        $this->PDU->setCentral($this->MOBILE_SMS_CENTRAL);
        $this->PDU->setSmsClass("0");
        $PDU_STRING = $this->PDU->getPduMessage($this->MOBILE_DESTINATION);
        $RES = $this->GSM->sendSms($PDU_STRING, $this->PDU->getPduSendLength());
        $this->assertStringStartsWith("+CMGS:", $RES[2]);
    }

    /**
     * This test requires PDU to have messages stored.
     */
    function testPduList($return = false)
    {
        $messageList = $this->GSM->CMGL();
        if ($return) {
            return $messageList;
        } else {
            $this->assertTrue(is_array($messageList) && count($messageList));
        }
    }

    /**
     * Test PDU reading, by first send message if pdu is empty
     */
    function testPduRead()
    {
        $assertMessage = "This is a message created by a self test " . time();
        $this->GSM->sendSms($this->MOBILE_SELF, $assertMessage);
        sleep(3);
        $messages = $this->testPduList(true);
        $message = array_pop($messages);
        if (isset($message[4])) {
            /** @var $pduInfo PDULIB_DATA_CONTENT */
            $pduInfo = $this->PDU->getDecodedPduString($message[4], $message[3]);
            $this->assertTrue($pduInfo->getSmsSender() == $this->MOBILE_SELF || $pduInfo->getSmsSender() == $this->MOBILE_DESTINATION);
        }
    }

	/**
	 * @test
	 *
	 * Send to 0707303844 (via +46708000708): Det gick inte med 3 --- Successful!
	 * PDU Receipt string: 0006260A817070038344813002819332408130028193424000
	 *
	 */
    function pduReceipt() {

    }

}
