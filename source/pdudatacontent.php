<?php

namespace TorneLIB;

/**
 * Class PDULIB_DATA_CONTENT
 * @package TorneLIB
 */
class PDULIB_DATA_CONTENT
{

    /** @var string $SMS_CENTRAL SMS Central used when receiving message */
    protected $SMS_CENTRAL;

    /** @var string $SMS_DELIVER_BITS DeliveryBits */
    protected $SMS_DELIVER_BITS;

    /** @var string $SMS_TIMESTAMP_UNIXTIME Timestamp of message in time() */
    protected $SMS_TIMESTAMP_UNIXTIME;

    /** @var string $SMS_SENDER Who sent this? */
    protected $SMS_SENDER;

    /** @var string $SMS_PROTOCOL_IDENTIFIER Protocol identifier */
    protected $SMS_PROTOCOL_IDENTIFIER;

    /** @var string $SMS_DATA_ENCODING Encoding used in message */
    protected $SMS_DATA_ENCODING;

    /** @var string $SMS_DATA_SIZE SMS message length */
    protected $SMS_DATA_SIZE;

    /** @var string $SMS_DATA_PART The SMS data part */
    protected $SMS_DATA_PART;

    /** @var PDULIB_DATA_UDH $SMS_DATA_UDH */
    protected $SMS_DATA_UDH;

    /** @var string $SMS_DATA_MESSAGE The message */
    protected $SMS_DATA_MESSAGE;

    function __construct($SMS_CENTRAL, $SMS_DELIVER_BITS, $SMS_TIMESTAMP_UNIXTIME, $SMS_SENDER, $SMS_PROTOCOL_IDENTIFIER, $SMS_DATA_ENCODING, $SMS_DATA_SIZE, $SMS_DATA_PART, $SMS_DATA_UDH, $SMS_DATA_MESSAGE)
    {
        $this->SMS_CENTRAL = $SMS_CENTRAL;
        $this->SMS_DELIVER_BITS = $SMS_DELIVER_BITS;
        $this->SMS_TIMESTAMP_UNIXTIME = $SMS_TIMESTAMP_UNIXTIME;
        $this->SMS_SENDER = $SMS_SENDER;
        $this->SMS_PROTOCOL_IDENTIFIER = $SMS_PROTOCOL_IDENTIFIER;
        $this->SMS_DATA_ENCODING = $SMS_DATA_ENCODING;
        $this->SMS_DATA_SIZE = $SMS_DATA_SIZE;
        $this->SMS_DATA_PART = $SMS_DATA_PART;
        $this->SMS_DATA_UDH = $SMS_DATA_UDH;
        $this->SMS_DATA_MESSAGE = $SMS_DATA_MESSAGE;
    }

    /**
     * @return string
     */
    public function getSmsCentral()
    {
        return $this->SMS_CENTRAL;
    }

    /**
     * @return string
     */
    public function getSmsDeliverBits()
    {
        return $this->SMS_DELIVER_BITS;
    }

    /**
     * @param bool $unixtime
     * @param string $timeFormat
     * @return string
     */
    public function getSmsTimeStamp($unixtime = true, $timeFormat = '%Y%m%d, %H:%M:%S')
    {
        if ($unixtime) {
            return $this->SMS_TIMESTAMP_UNIXTIME;
        } else {
            return strftime($timeFormat, $this->SMS_TIMESTAMP_UNIXTIME);
        }
    }

    /**
     * @return string
     */
    public function getSmsSender()
    {
        return $this->SMS_SENDER;
    }

    /**
     * @return string
     */
    public function getSmsProtocolIdentifier()
    {
        return $this->SMS_PROTOCOL_IDENTIFIER;
    }

    /**
     * @return string
     */
    public function getSmsDataEncoding()
    {
        return $this->SMS_DATA_ENCODING;
    }

    /**
     * @return string
     */
    public function getSmsDataSize()
    {
        return $this->SMS_DATA_SIZE;
    }

    /**
     * @return string
     */
    public function getSmsDataPart()
    {
        return $this->SMS_DATA_PART;
    }

    /**
     * @return PDULIB_DATA_UDH
     */
    public function getSmsDataUdh()
    {
        return $this->SMS_DATA_UDH;
    }

    /**
     * @return string
     */
    public function getSmsDataMessage()
    {
        return $this->SMS_DATA_MESSAGE;
    }



}
