<?php

namespace TorneLIB;

use TorneLIB\TorneLIB_PDU;

//require_once(__DIR__ . '/../deprecation/gsm.functions.php');
require_once(__DIR__ . '/pdulib.php');

// Indexed incoming
// +CMTI: "SM",7

class GSM
{

	private $DEVICE = "/dev/ttyS0";
	private $MODEM;
	private $WAIT_STREAM = 10;
	private $MODEM_MODE = MODEM_MODES::MODE_TEXT;

	private $PDU_ENCODER;

	function __construct($DEVICE = '/dev/ttyS0')
	{
		$this->DEVICE = $DEVICE;
		$this->PDU_ENCODER = new TorneLIB_PDU();
		if (!file_exists($DEVICE)) {
			throw new \Exception("Device " . $DEVICE . " not found!");
		}
	}

	function __destruct()
	{
		@fclose($this->MODEM);
	}

	public function connect($nonBlockingMode = true)
	{
		if (file_exists($this->DEVICE)) {
			//system("stty -F ".$this->DEVICE." 115200");
			$this->MODEM = fopen($this->DEVICE, "r+b");
			stream_set_timeout($this->MODEM, 5);
		} else {
			throw new \Exception("Device " . $this->DEVICE . " not found!");
		}
		if (is_resource($this->MODEM)) {
			$this->NONBLOCKING = $nonBlockingMode;
			if ($nonBlockingMode) {
				stream_set_blocking($this->MODEM, 0);
			} else {
				stream_set_blocking($this->MODEM, 1);
			}

			return true;
		}

		return false;
	}

	public function disconnect()
	{
		@fclose($this->MODEM);
	}

	public function send($dataIn = '', $crLf = true, $wait = false)
	{
		if (empty($this->MODEM)) {
			$this->connect();
		}
		$preparedDataIn = $dataIn . ($crLf ? "\r\n" : "");
		fwrite($this->MODEM, $preparedDataIn);
		if ($wait) {
			$modemData = $this->getModemData();

			return $modemData;
		}
	}

	public function getModemData()
	{
		$waitStream = 0;
		$dataArray = array();

		sleep(3);

		while (($dataStream = fread($this->MODEM, 4096)) !== false) {
			preg_match_all("/(.*?)\n/s", $dataStream, $dataOut);

			if (isset($dataOut[1])) {
				$dataStreamArray = $dataOut[1];
				foreach ($dataStreamArray as $row) {
					$isRow = trim($row);
					if (!empty($isRow)) {
						$dataArray[] = $isRow;
					}
				}
				break;
			}
		}
		return $dataArray;
	}

	public function setWaitStream($waitCount = 10)
	{
		$this->WAIT_STREAM = $waitCount;
	}

	public function AT($atCmd = '', $wait = false)
	{
		return $this->send("AT" . $atCmd, true, $wait);
	}

	private function getIsTrue($dataStream = array(), $row = 1)
	{
		if (isset($dataStream[$row]) && $dataStream[$row] == "OK") {
			return true;
		}

		return false;
	}

	public function setMode($modemMode = MODEM_MODES::MODE_TEXT)
	{
		$this->MODEM_MODE = $modemMode;
		$successMode = $this->getIsTrue($this->AT("+CMGF=" . $this->MODEM_MODE, true));
		if ($successMode) {
			$this->MODEM_MODE = $modemMode;
		}

		return $successMode;
	}

	/**
	 * New message notification AT+CNMI
	 *
	 * @link http://www.logicbus.com.mx/pdf/31/Comandos_AT_generales.pdf
	 */
	public function setMessageIndication()
	{
		$CNMI = $this->AT("+CNMI=2,0,0,1,0", true);
		if (isset($CNMI[1])) {
			return $CNMI[1] == "OK" ? true : false;
		}

		return null;
	}

	/**
	 * @return int
	 */
	public function getMode()
	{
		return $this->MODEM_MODE;
	}

	public function sendSms($destinationOrPdu = '', $smsMessageOrLength = '', $smsClass = '')
	{
		if ($this->getMode() === MODEM_MODES::MODE_TEXT) {
			if (!empty($destinationOrPdu) && !empty($smsMessageOrLength)) {
				// ("GSM","PCCP437","CUSTOM","HEX")
				$this->CSCS('="PCCP437"');
				$codepage437 = iconv("UTF-8", "CP437", $smsMessageOrLength);
				sleep(1);
				$smsMessageResult = $this->AT("+CMGS=" . $destinationOrPdu . "\r" . $codepage437 . chr(26), true);

				foreach ($smsMessageResult as $msgResult) {
					if (preg_match("/^\+CMGS/", $msgResult)) {
						return true;
					}
				}

				//return count($smsMessageResult) >= 3 && $smsMessageResult[3] == "OK" ? true : false;
			}
		} else {
			return $this->AT("+CMGS=" . $smsMessageOrLength . "\r" . $destinationOrPdu . chr(26), true);
		}
		throw new \Exception("sendSms requires destination and message");
	}

	public function COPS($arg = '?')
	{
		$COPS = $this->AT('+COPS' . $arg, true);
		if (isset($COPS[1])) {
			return $COPS[1];
		}

		return null;
	}

	public function CREG($arg = '?')
	{
		$CREG = $this->AT('+CREG' . $arg, true);
		if (isset($CREG[1])) {
			return $CREG[1];
		}

		return null;
	}

	public function CIMI($arg = '')
	{
		$CIMI = $this->AT('+CIMI' . $arg, true);
		if (isset($CIMI[1])) {
			return $CIMI[1];
		}

		return null;
	}

	public function CSQ($arg = '')
	{
		$CSQ = $this->AT('+CSQ' . $arg, true);
		if (isset($CSQ[1])) {
			return $CSQ[1];
		}

		return null;
	}

	public function CSCS($arg = '?')
	{
		$CSCS = $this->AT('+CSCS' . $arg, true);
		if (isset($CSCS[1])) {
			return $CSCS[1];
		}

		return null;
	}

	private function CMG($arg = '')
	{
		$this->setMode(MODEM_MODES::MODE_PDU);
		$CMG = $this->AT('+CMG' . $arg, true);
		$messageList = array();
		$nextRowIsMessage = false;

		// Handle messages to read here
		if (isset($CMG[1]) && preg_match("/^\+CMGL/", $CMG[1])) {
			foreach ($CMG as $List) {
				if ($nextRowIsMessage) {

					// Merge the full data array
					$pduDataInfo[] = $List;

					// Decoding this here might fail.
					$pduDataInfo[] = $this->PDU_ENCODER->getDecodedPduString($pduDataInfo[4], $pduDataInfo[3]);

					// If everything is correctly set, the message should be stored in part 4 (counted from 0) - complete array size should be 5.
					$messageList[] = $pduDataInfo;

					// Disable storing next row from PDU
					$nextRowIsMessage = false;

					// Reset the array to make sure nothing follows if something fails
					$pduDataInfo = array();
				}
				if (preg_match("/^\+CMGL/", $List)) {
					// Extract current indexing data from upcoming message
					$listPduInfo = preg_replace("/(.*?)\s+/", '', $List);

					// Make the data an array and check if the "correct" data is there
					$pduDataInfo = explode(",", $listPduInfo);
					if (isset($pduDataInfo[3])) {
						$nextRowIsMessage = true;
					}
				}
			}
		}

		return $messageList;
	}

	/**
	 * List all stored message on modem
	 *
	 * @return array
	 */
	public function CMGL()
	{
		return $this->CMG("L=4");
	}

	public function CMGR($pduMessageIndex)
	{
		return $this->CMG("R=" . $pduMessageIndex);
	}

}

abstract class MODEM_MODES
{
	const MODE_PDU = 0;
	const MODE_TEXT = 1;
}

