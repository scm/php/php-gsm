<?php

namespace TorneLIB;

/**
 * Class PDULIB_DATA_UDH
 * @package TorneLIB
 */
class PDULIB_DATA_UDH
{
    /** @var int $UDH_IDENTIFIER Message identifier */
    protected $UDH_IDENTIFIER = 0;

    /** @var int $UDH_LENGTH UDH data length */
    protected $UDH_LENGTH = 0;

    /** @var int $UDH_REFERENCE UDH Message reference (for combining messages) */
    protected $UDH_REFERENCE = 0;

    /** @var int $UDH_TOTAL_MESSAGES How many messages should be concatenated at end */
    protected $UDH_TOTAL_MESSAGES = 1;

    /** @var int $UDH_CURRENT_MESSAGE_ID Current message id */
    protected $UDH_CURRENT_MESSAGE_ID = 1;

    /** @var int Length of header */
    protected $UDH_HDR_LENGTH = 0;

    /**
     * PDULIB_DATA_UDH constructor.
     * @param $UDH_IDENTIFIER
     * @param $UDH_LENGTH
     * @param $UDH_REFERENCE
     * @param $UDH_TOTAL_MESSAGES
     * @param $UDH_CURRENT_MESSAGE_ID
     */
    function __construct($UDH_HDR_LENGTH = 0, $UDH_IDENTIFIER = 0, $UDH_LENGTH = 0, $UDH_REFERENCE = 0, $UDH_TOTAL_MESSAGES = 1, $UDH_CURRENT_MESSAGE_ID = 1)
    {
        $this->UDH_IDENTIFIER = $UDH_IDENTIFIER;
        $this->UDH_LENGTH = $UDH_LENGTH;
        $this->UDH_REFERENCE = $UDH_REFERENCE;
        $this->UDH_TOTAL_MESSAGES = $UDH_TOTAL_MESSAGES;
        $this->UDH_CURRENT_MESSAGE_ID = $UDH_CURRENT_MESSAGE_ID;
        $this->UDH_HDR_LENGTH = $UDH_HDR_LENGTH;
    }

    /**
     * Return message identifier
     *
     * @return int
     */
    public function getIdentifier()
    {
        return $this->isUdh() ? (int)$this->UDH_IDENTIFIER : 0;
    }

    /**
     * Return current lenght of user data header
     *
     * @return int
     */
    public function getLength()
    {
        return $this->isUdh() ? (int)$this->UDH_LENGTH : 0;
    }

    /**
     * Return the user data reference header reference for concatenation
     *
     * @return int
     */
    public function getReference()
    {
        return $this->isUdh() ? (int)$this->UDH_REFERENCE : 0;
    }

    /**
     * Return the current total of concatenated messages
     *
     * @return int
     */
    public function getTotalMessages()
    {
        return $this->isUdh() ? (int)$this->UDH_TOTAL_MESSAGES : 1;
    }

    /**
     * Return the current message for concatenation
     *
     * @return int
     */
    public function getCurrentMessage()
    {
        return $this->isUdh() ? (int)$this->UDH_CURRENT_MESSAGE_ID : 1;
    }

    public function getHdrLength()
    {
        return $this->UDH_HDR_LENGTH;
    }

    /**
     * Make sure that the user data header is treated properly. If there is no user data header, this should be returned false.
     *
     * @return bool
     */
    public function isUdh()
    {
        // Long shot of the UDH length. It should normally be 5-6 or shorter.
        if ($this->UDH_HDR_LENGTH <= 10) {
            return true;
        }
        return false;
    }

}
