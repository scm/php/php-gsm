<?php

/**
 * Copyright 2018 Tomas Tornevall & Tornevall Networks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package TorneLIB
 * @version 6.0.0
 */

namespace TorneLIB;

//require_once(__DIR__ . '/pdudatacontent.php');
//require_once(__DIR__ . '/pdudataudh.php');

if ( ! class_exists( 'TorneLIB_PDU' ) && ! class_exists( 'TorneLIB\TorneLIB_PDU' ) ) {

	/**
	 * Class PDU Simple GSM/PDU encoding library
	 *
	 * @package TorneLIB
	 * @version 6.0.0
	 */
	class TorneLIB_PDU {
		private $PRE_GSM_CHARSET;

		/** @var array 7 bit encoding table */
		private $BIT7TABLE = array(
			'@',
			'£',
			'$',
			'¥',
			'è',
			'é',
			'ù',
			'ì',
			'ò',
			'Ç',
			'\n',
			'Ø',
			'ø',
			'\r',
			'Å',
			'å',
			'\u0394',
			'_',
			'\u03a6',
			'\u0393',
			'\u039b',
			'\u03a9',
			'\u03a0',
			'\u03a8',
			'\u03a3',
			'\u0398',
			'\u039e',
			'.',
			'Æ',
			'æ',
			'ß',
			'É',
			' ',
			'!',
			'"',
			'#',
			'¤',
			'%',
			'&',
			'\'',
			'(',
			')',
			'*',
			'+',
			',',
			'-',
			'.',
			'/',
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			':',
			';',
			'<',
			'=',
			'>',
			'?',
			'¡',
			'A',
			'B',
			'C',
			'D',
			'E',
			'F',
			'G',
			'H',
			'I',
			'J',
			'K',
			'L',
			'M',
			'N',
			'O',
			'P',
			'Q',
			'R',
			'S',
			'T',
			'U',
			'V',
			'W',
			'X',
			'Y',
			'Z',
			'Ä',
			'Ö',
			'Ñ',
			'Ü',
			'§',
			'¿',
			'a',
			'b',
			'c',
			'd',
			'e',
			'f',
			'g',
			'h',
			'i',
			'j',
			'k',
			'l',
			'm',
			'n',
			'o',
			'p',
			'q',
			'r',
			's',
			't',
			'u',
			'v',
			'w',
			'x',
			'y',
			'z',
			'ä',
			'ö',
			'ñ',
			'ü',
			'à'
		);
		private $GSM_CHARSET;

		private $_SMS_CENTRAL;
		private $SMS_CENTRAL;
		private $SMS_CENTRAL_FORMAT;
		private $SMS_CENTRAL_INFO_LENGTH;
		private $SMS_CENTRAL_LENGTH;
		private $PDU_SMS_STRING;
		private $PDU_SMS_MESSAGE;
		private $PDU_SMS_CLASS = "-1";
		private $PDU_SMS_RECEIPT = true;
		private $PDU_SMS_VALIDITY = false;

		/** @var PDULIB_DATA_UDH $PDU_SMS_UDH User data header storage */
		private $PDU_SMS_UDH;
		private $PDU_SMS_TRANSFORMED;
		private $PDU_SMS_POSITION;
		private $PDU_INTERNAL_CHARSET = false;
		private $PDU_INTERNAL_PADDING = false;

		// DECODING
		private $PDUBYTES_ARRAY_OCTETS = array();
		private $PDUBYTES_ARRAY_REST = array();
		private $PDUBYTES_ARRAY_SEPTETS = array();

		function __construct() {
			if ( file_exists( __DIR__ . "/GSM0338.TXT" ) ) {
				$this->PRE_GSM_CHARSET = explode( "\n", file_get_contents( __DIR__ . "/GSM0338.TXT" ) );
			}
			$this->getGsmCharset();
		}

		private function strToHex( $string ) {
			$hex = '';
			for ( $i = 0; $i < strlen( $string ); $i ++ ) {
				$hex .= dechex( ord( $string[ $i ] ) );
			}

			return $hex;
		}

		private function hexToStr( $hex ) {
			$string = '';
			for ( $i = 0; $i < strlen( $hex ) - 1; $i += 2 ) {
				$string .= chr( hexdec( $hex[ $i ] . $hex[ $i + 1 ] ) );
			}

			return $string;
		}

		private function hexbin( $hex ) {
			$bin = '';
			for ( $i = 0; $i < strlen( $hex ); $i ++ ) {
				$bin .= str_pad( decbin( hexdec( $hex{$i} ) ), 4, '0', STR_PAD_LEFT );
			}

			return $bin;
		}

		private function binhex( $bin ) {
			$hex = '';
			for ( $i = strlen( $bin ) - 4; $i >= 0; $i -= 4 ) {
				$hex .= dechex( bindec( substr( $bin, $i, 4 ) ) );
			}

			return strrev( $hex );
		}

		private function bit7( $string ) {
			return $this->bit8to2( $string );
		}

		private function bit8to2( $string ) {
			$strlen      = strlen( $string );
			$octetSecond = '';
			$output      = '';
			for ( $i = 0; $i <= $strlen; $i ++ ) {
				if ( $i == $strlen ) {
					if ( $octetSecond != "" ) {
						$b2d = dechex( bindec( $octetSecond ) );
						if ( strlen( $b2d ) == 1 ) {
							$b2d = "0" . $b2d;
						}
						$output = $output . $b2d;
						break;
					}
				}
				$current = $this->intToBin( $this->getSevenBit( ( isset( $string{$i} ) ? $string{$i} : "" ) ), 7 );
				if ( $i != 0 && $i % 8 != 0 ) {
					$octetFirst   = substr( $current, 7 - ( $i ) % 8 );
					$currentOctet = $octetFirst . $octetSecond;
					$d2h          = dechex( bindec( $currentOctet ) );
					if ( strlen( $d2h ) == 1 ) {
						$d2h = "0" . $d2h;
					}
					$output      = $output . $d2h;
					$octetSecond = substr( $current, 0, 7 - ( $i ) % 8 );
				} else {
					$octetSecond = substr( $current, 0, 7 - ( $i ) % 8 );
				}
			}

			return $output;
		}

		private function semiOctetToString( $string ) {
			$out = '';
			for ( $i = 0; $i < strlen( $string ); $i += 2 ) {
				$temp = substr( $string, $i, $i + 2 );
				$out  .= $this->phoneNumberMap( isset( $temp{1} ) ? $temp{1} : "" ) . $this->phoneNumberMap( isset( $temp{0} ) ? $temp{0} : "" );
			}

			return $out;
		}

		private function parseInt( $string ) {
			if ( preg_match( '/(\d+)/', $string, $array ) ) {
				return $array[1];
			} else {
				return 0;
			}
		}

		private function str2bin( $str ) {
			$out = false;
			for ( $a = 0; $a < strlen( $str ); $a ++ ) {
				$dec = ord( substr( $str, $a, 1 ) );
				$bin = '';
				for ( $i = 7; $i >= 0; $i -- ) {
					if ( $dec >= pow( 2, $i ) ) {
						$bin .= "1";
						$dec -= pow( 2, $i );
					} else {
						$bin .= "0";
					}
				}
				$out .= $bin;
			}

			return $out;
		}

		private function intToBin( $x, $size ) {
			$num = $this->parseInt( $x );
			$bin = decbin( $num );
			for ( $i = strlen( $bin ); $i < $size; $i ++ ) {
				$bin = "0" . $bin;
			}

			return $bin;
		}

		private function getSevenBinDec( $inVal ) {
			$binaryDecimal = bindec( $inVal );
			if ( isset( $this->GSM_CHARSET[ $binaryDecimal ] ) ) {
				return $this->GSM_CHARSET[ $binaryDecimal ];

			}

			return;
		}

		private function getGsmCharset() {
			if ( $this->PDU_INTERNAL_CHARSET ) {
				$this->GSM_CHARSET = $this->BIT7TABLE;

				return;
			}
			$sevenbitdefault   = array();
			$this->GSM_CHARSET = $this->BIT7TABLE;
			if ( ! count( $this->PRE_GSM_CHARSET ) ) {
				$this->GSM_CHARSET = $this->BIT7TABLE;
			} else {
				if ( is_array( $this->PRE_GSM_CHARSET ) && count( $this->PRE_GSM_CHARSET ) ) {
					foreach ( $this->PRE_GSM_CHARSET as $row ) {
						if ( ! preg_match( "/^#/", trim( $row ) ) ) {
							$row = explode( " ", preg_replace( "/\s/", ' ', $row ) );
							if ( $row[0] != "" ) {
								$int    = hexdec( preg_replace( "/^0x/", '', $row[0] ) );
								$tochar = chr( hexdec( preg_replace( "/^0x/", '', $row[1] ) ) );
								//$sevenbitdefault[$int] = utf8_encode($tochar);
								$sevenbitdefault[ $int ] = $tochar;
							}
						}
					}
				}
			}
			$this->GSM_CHARSET = $sevenbitdefault;
		}

		private function getSevenBit( $char ) {
			for ( $i = 0; $i < count( $this->GSM_CHARSET ); $i ++ ) {
				if ( isset( $this->GSM_CHARSET[ $i ] ) ) {
					if ( $this->GSM_CHARSET[ $i ] == $char ) {
						return $i;
					}
				}
			}

			return 0;
		}

		private function getEightBit( $char ) {
			return $char;
		}

		private function get16Bit( $char ) {
			return $char;
		}

		function dechex_str( $ref ) {
			return ( $ref <= 15 ) ? '0' . dechex( $ref ) : dechex( $ref );
		}

		function phoneNumberMap( $char ) {
			if ( $char >= "0" && $char <= "9" ) {
				return $char;
			}
			switch ( strtoupper( $char ) ) {
				case "*":
					return "A";
					break;
				case "#":
					return "B";
					break;
				case "A":
					return "C";
					break;
				case "B":
					return "D";
					break;
				case "C":
					return "E";
					break;
				default:
					return 'F';
			}

			return "F";
		}

		/**
		 * Set up a SMS central to use in PDU mode
		 *
		 * @param $smsCentralNumber
		 */
		public function setCentral( $smsCentralNumber ) {
			$this->_SMS_CENTRAL = $smsCentralNumber;
			$this->setSmsCentral( $smsCentralNumber );
		}

		/**
		 * Get SMS central set
		 *
		 * @return string
		 */
		public function getCentral() {
			return $this->_SMS_CENTRAL;
		}

		/**
		 * Prepare SMS
		 *
		 * This method has currently no effect as concatenated message requires split at 140 characters (TODO)
		 *
		 * @param string $message
		 */
		public function setPduSmsMessage( $message = '' ) {
			$utfMessage   = utf8_decode( $message );
			$arrayMessage = array( 'message' => array(), 'messagePdu' => array() );
			$length       = strlen( $utfMessage );
			$messages     = ceil( $length / 150 );
			for ( $i = 0; $i < $messages; $i ++ ) {
				$preparedMessage = substr( $utfMessage, 0, 159 );;
				$arrayMessage['message'][]    = $preparedMessage;
				$arrayMessage['messagePdu'][] = strtoupper( $this->bit7( $preparedMessage ) );
				$utfMessage                   = substr( $utfMessage, 160 );
			}
			$this->PDU_SMS_MESSAGE = $arrayMessage;
		}

		public function getPduSmsMessage() {
			return $this->PDU_SMS_MESSAGE;
		}

		public function setSmsClass( $SMSCLASS = "-1" ) {
			$this->PDU_SMS_CLASS = $SMSCLASS;
		}

		public function getSmsClass() {
			return $this->PDU_SMS_CLASS;
		}

		public function setReceipt( $requestReceipt = true ) {
			$this->PDU_SMS_RECEIPT  = $requestReceipt;
			$this->PDU_SMS_VALIDITY = false;
		}

		public function getReceipt() {
			return $this->PDU_SMS_RECEIPT;
		}

		private function setSmsCentral( $smsCentralNumber = '' ) {
			if ( strlen( $smsCentralNumber ) != 0 ) {
				$this->SMS_CENTRAL_FORMAT = "81";
				if ( substr( $smsCentralNumber, 0, 1 ) == "+" ) {
					$this->SMS_CENTRAL_FORMAT = "91";
					$smsCentralNumber         = substr( $smsCentralNumber, 1 );
				} else if ( substr( $smsCentralNumber, 0, 1 ) != "0" ) {
					$this->SMS_CENTRAL_FORMAT = "91";
				}
				if ( strlen( $smsCentralNumber ) % 2 != 0 ) {
					$smsCentralNumber .= "F";
				}
			}

			$SMS_CENTRAL = $this->semiOctetToString( $smsCentralNumber );
			$this->setSmsCentralLength( $SMS_CENTRAL );
			$this->SMS_CENTRAL = $SMS_CENTRAL;

			return $SMS_CENTRAL;
		}

		private function setSmsCentralLength( $SMS_CENTRAL ) {
			$smsc_info_length = strlen( $this->SMS_CENTRAL_FORMAT . $SMS_CENTRAL ) / 2;
			$smsc_length      = $smsc_info_length;
			if ( $smsc_info_length < 10 ) {
				$smsc_info_length = "0" . $smsc_info_length;
			}
			$this->SMS_CENTRAL_LENGTH      = $smsc_length;
			$this->SMS_CENTRAL_INFO_LENGTH = $smsc_info_length;
		}

		private function getSmsReceiptOctets() {
			if ( $this->getReceipt() ) {
				if ( $this->PDU_SMS_VALIDITY ) {
					$returnOctet = "3100";
				} else {
					$returnOctet = "2100";
				}
			} else {
				if ( $this->PDU_SMS_VALIDITY ) {
					$returnOctet = "1100";
				} else {
					$returnOctet = "0100";
				}
			}

			return $returnOctet;
		}

		private function getDestinationOctets( $destinationNumber ) {
			$destination_number_format = "81";
			if ( substr( $destinationNumber, 0, 1 ) == "+" ) {
				$destination_number_format = "91";
				$destinationNumber         = substr( $destinationNumber, 1 );
			} else if ( substr( $destinationNumber, 0, 1 ) != "0" ) {
				$destination_number_format = "91";
			}
			$destination_number_length = dechex( strlen( $destinationNumber ) );
			if ( strlen( $destinationNumber ) % 2 != 0 ) {
				$destinationNumber .= "F";
			}
			if ( strlen( $destination_number_length ) < 10 ) {
				$destination_number_length = "0" . $destination_number_length;
			}
			$destinationNumber                   = $this->semiOctetToString( $destinationNumber );
			$octetCollection                     = $destination_number_length . $destination_number_format . $destinationNumber;
			$this->SMS_DESTINATION_NUMBER_LENGTH = $destination_number_length;
			$this->SMS_DESTINATION_NUMBER_FORMAT = $destination_number_format;

			return $octetCollection;
		}

		public function getPduSendLength() {
			return ( strlen( $this->PDU_SMS_STRING ) / 2 - $this->SMS_CENTRAL_INFO_LENGTH - 1 );
		}

		public function getPduMessage( $destinationNumber = '' ) {
			$currentMessage = "";
			if ( empty( $this->_SMS_CENTRAL ) ) {
				throw new \Exception( "SMSC Required - use setCentral()" );
			}
			if ( ! count( $this->PDU_SMS_MESSAGE ) ) {
				throw new \Exception( "SMS Message is empty - use setPduSmsMessage()" );
			}
			if ( empty( trim( $destinationNumber ) ) ) {
				throw new \Exception( "SMS Destination number not set" );
			}

			if ( count( $this->PDU_SMS_MESSAGE['messagePdu'] ) == 1 ) {
				$currentMessage    = array_pop( $this->PDU_SMS_MESSAGE['message'] );
				$currentPduMessage = array_pop( $this->PDU_SMS_MESSAGE['messagePdu'] );
			} else {
				// TODO: Concatenation
			}

			$SMS_PROTO_ID = "00";

			$DCS = 0;
			if ( $this->PDU_SMS_CLASS != "-1" ) {
				$DCS = $this->PDU_SMS_CLASS + 16;
			}
			$SMS_DATA_ENCODING = dechex( $DCS );
			if ( strlen( $SMS_DATA_ENCODING ) == 1 ) {
				$SMS_DATA_ENCODING = "0" . $SMS_DATA_ENCODING;
			}
			$SMS_VALID_PERIOD = null;       // TODO: Include validity
			if ( $this->PDU_SMS_VALIDITY == 1 ) {
				$SMS_VALID_PERIOD = dechex( $SMS_VALID_PERIOD );
			}

			$SMS_REFERENCE     = 1;     // TODO: DYNAMIC
			$SMS_MESSAGE_COUNT = 1;
			$SMS_MESSAGE_PART  = 1;

			$SMS_UDH              = array(
				'udh_length'    => '05', //sms udh length 05 for 8bit udh, 06 for 16 bit udh
				'identifier'    => '00', //use 00 for 8bit udh, use 08 for 16bit udh
				'header_length' => '03', //length of header including udh_length & identifier
				'reference'     => '1', //use 2bit 00-ff if 8bit udh, use 4bit 0000-ffff if 16bit udh
				'msg_count'     => '1', //sms count
				'msg_part'      => '1' //sms part number
			);
			$SMS_UDH['reference'] = $this->dechex_str( $SMS_REFERENCE );
			$SMS_UDH['msg_count'] = $this->dechex_str( $SMS_MESSAGE_COUNT );
			$SMS_UDH['msg_part']  = $this->dechex_str( $SMS_MESSAGE_PART );

			$SMS_UDH_STRING     = implode( '', $SMS_UDH );
			$SMS_USER_DATA_SIZE = base_convert( strlen( $currentMessage ), 10, 16 );
			if ( strlen( $SMS_USER_DATA_SIZE ) == 1 ) {
				$SMS_USER_DATA_SIZE = "0" . $SMS_USER_DATA_SIZE;
			}

			if ( ! $this->PDU_INTERNAL_PADDING ) {
				$currentUdhMessage = $SMS_USER_DATA_SIZE . $currentPduMessage;
			} else {
				// TODO: Using UDH requires padding (Experimental)
				//$currentPaddedString = strtoupper($this->uglyPad($SMS_UDH, $currentPduMessage));
				//$currentUdhMessage = $SMS_USER_DATA_SIZE . $currentPaddedString;
			}

			$SMS_COMPILED_HEADER = array(
				$this->SMS_CENTRAL_INFO_LENGTH,
				$this->SMS_CENTRAL_FORMAT,
				$this->SMS_CENTRAL,
				$this->getSmsReceiptOctets(),
				$this->getDestinationOctets( $destinationNumber ),
				$SMS_PROTO_ID,
				$SMS_DATA_ENCODING,
				$SMS_VALID_PERIOD,
				$currentUdhMessage
			);

			$PDU_MESSAGE          = implode( '', array_map( 'strtoupper', $SMS_COMPILED_HEADER ) );
			$this->PDU_SMS_STRING = $PDU_MESSAGE;

			return $PDU_MESSAGE;
		}

		private function getByteString( $pduDataString = '' ) {
			// Extract octets
			$byteString = '';
			for ( $i = 0; $i < strlen( $pduDataString ); $i = $i + 2 ) {
				$hex        = substr( $pduDataString, $i, 2 );
				$byteString .= $this->intToBin( hexdec( $hex ), 8 );
			}

			return $byteString;
		}

		private function getPduByteArray( $byteString = '' ) {
			$count = 0;
			$s     = 1;
			// Reset arrays in case of old data
			$this->PDUBYTES_ARRAY_OCTETS  = array();
			$this->PDUBYTES_ARRAY_REST    = array();
			$this->PDUBYTES_ARRAY_SEPTETS = array();
			for ( $i = 0; $i < strlen( $byteString ); $i = $i + 8 ) {
				$this->PDUBYTES_ARRAY_OCTETS[ $count ]  = substr( $byteString, $i, 8 );
				$this->PDUBYTES_ARRAY_REST[ $count ]    = substr( $this->PDUBYTES_ARRAY_OCTETS[ $count ], 0, ( $s % 8 ) );
				$this->PDUBYTES_ARRAY_SEPTETS[ $count ] = substr( $this->PDUBYTES_ARRAY_OCTETS[ $count ], ( $s % 8 ), 8 );
				$s ++;
				$count ++;
				if ( $s == 8 ) {
					$s = 1;
				}
			}
		}

		/**
		 * @param string $pduDataInputString
		 *
		 * @return \PDULIB_DATA_UDH
		 */
		private function getUdh( $pduDataInputString = '' ) {
			$hdrlen   = hexdec( substr( $pduDataInputString, 0, 2 ) ) + 1;    // Include lengthdatainfo
			$header   = substr( $pduDataInputString, 2, $hdrlen * 2 );        // Grab header
			$hdrCount = 1;
			for ( $i = 0; $i <= strlen( $header ); $i = $i + 2 ) {
				$hdrhex = substr( $header, $i, 2 );
				if ( $hdrCount == 1 ) {
					$udhIdentifier = hexdec( $hdrhex );
				}
				if ( $hdrCount == 2 ) {
					$udhLength = hexdec( $hdrhex );
				}
				if ( $hdrCount == 3 ) {
					$udhReference = hexdec( $hdrhex );
				}
				if ( $hdrCount == 4 ) {
					$udhTotal = hexdec( $hdrhex );
				}
				if ( $hdrCount == 5 ) {
					$udhCurrentPart = hexdec( $hdrhex );
					break;
				}
				$hdrCount ++;
			}

			if ( $hdrCount && isset( $udhCurrentPart ) ) {
				$this->PDU_SMS_UDH = new PDULIB_DATA_UDH( $hdrlen, $udhIdentifier, $udhLength, $udhReference, $udhTotal, $udhCurrentPart );
			} else {
				$this->PDU_SMS_UDH = new PDULIB_DATA_UDH();
			}

			return $this->PDU_SMS_UDH;
		}

		private function getMessageTransformed() {
			$smsMessage = "";
			$matchCount = 0;

			for ( $i = 0; $i < count( $this->PDUBYTES_ARRAY_REST ); $i ++ ) {
				if ( $i % 7 == 0 ) {
					if ( $i != 0 ) {
						$smsMessage .= $this->getSevenBinDec( $this->PDUBYTES_ARRAY_REST[ $i - 1 ] );
						$matchCount ++;
					}
					if ( ! $this->PDU_SMS_UDH->isUdh() ) {
						$smsMessage .= $this->getSevenBinDec( $this->PDUBYTES_ARRAY_SEPTETS[ $i ] );
					} else {
						if ( $matchCount > $this->PDU_SMS_UDH->getHdrLength() ) {
							$smsMessage .= $this->getSevenBinDec( $this->PDUBYTES_ARRAY_SEPTETS[ $i ] );
						}
					}
					$matchCount ++;
				} else {
					if ( ! $this->PDU_SMS_UDH->isUdh() ) {
						$smsMessage .= $this->getSevenBinDec( $this->PDUBYTES_ARRAY_SEPTETS[ $i ] . $this->PDUBYTES_ARRAY_REST[ $i - 1 ] ); // There was a + here
					} else {
						if ( $matchCount > $this->PDU_SMS_UDH->getHdrLength() ) {
							$smsMessage .= $this->getSevenBinDec( $this->PDUBYTES_ARRAY_SEPTETS[ $i ] . $this->PDUBYTES_ARRAY_REST[ $i - 1 ] ); // There was a + here
						}
					}
					$matchCount ++;
				}
			}

			$this->PDU_SMS_TRANSFORMED = $smsMessage;

			return $this->PDU_SMS_TRANSFORMED;
		}

		function setPositionIncrease( $positionCount = 1, $pduDataArray = array() ) {
			$hasReturn = null;
			if ( count( $pduDataArray ) ) {
				$hasReturn = isset( $pduDataArray[ $this->PDU_SMS_POSITION ] ) ? $pduDataArray[ $this->PDU_SMS_POSITION ] : "";
			}
			$this->PDU_SMS_POSITION += $positionCount;
			if ( ! is_null( $hasReturn ) ) {
				return $hasReturn;
			}
		}

		/**
		 * Decoder for receipts
		 *
		 * @param string $pduReceiptString
		 * @link http://www.logicbus.com.mx/pdf/31/Comandos_AT_generales.pdf
		 */
		public function getDecodedPduReceipt($pduReceiptString = '') {

		}

		/**
		 * @param string $pduDataString
		 * @param string $pduDataLength
		 *
		 * @return PDULIB_DATA_CONTENT
		 * @throws \Exception
		 */
		public function getDecodedPduString( $pduDataString = '', $pduDataLength = '' ) {
			if ( intval( $pduDataLength ) === 0 ) {
				throw new \Exception( "PDU data lenght must be included" );
			}

			// Prepare SMS reading
			preg_match_all( "/[0-9A-Z]{2}/i", $pduDataString, $out );
			$hexcontent = $out[0];

			$smsCentralInfoLength = hexdec( $hexcontent[0] );
			$smsCentralNum        = "";
			for ( $this->PDU_SMS_POSITION = 1; $this->PDU_SMS_POSITION <= $smsCentralInfoLength; $this->PDU_SMS_POSITION ++ ) {
				if ( $this->PDU_SMS_POSITION == 1 ) {
					$numformat = ( $hexcontent[ $this->PDU_SMS_POSITION ] == "91" ? "+" : "0" );
				} else {
					$smsCentralNum .= $hexcontent[ $this->PDU_SMS_POSITION ];
				}
			}
			$SMS_CENTRAL      = preg_replace( "[F$]", '', $numformat . strtoupper( $this->semiOctetToString( $smsCentralNum ) ) );
			$SMS_DELIVER_BITS = $this->setPositionIncrease( 1, $hexcontent );
			$SMS_TIMESTAMP    = "";

			$sender_info_len = hexdec( $this->setPositionIncrease( 1, $hexcontent ) );
			$numFormatTest   = $this->setPositionIncrease( 1, $hexcontent );
			$numformat       = $numFormatTest == "91" ? "+" : "0";

			$SMS_SENDER = "";
			for ( $currentContentPosition = 1; $currentContentPosition <= $sender_info_len; $currentContentPosition ++ ) {
				$countchars = $currentContentPosition * 2;
				$SMS_SENDER .= $this->setPositionIncrease( 1, $hexcontent );
				if ( $countchars >= $sender_info_len ) {
					break;
				}
			}
			$SMS_SENDER = preg_replace( "[F$]", '', $numformat . strtoupper( $this->semiOctetToString( $SMS_SENDER ) ) );
			if ( preg_match( "[A-Z]", $SMS_SENDER ) ) {
				$this->PDU_SMS_POSITION --;
			}
			$SMS_PROTOCOL_IDENTIFIER = $this->setPositionIncrease( 1, $hexcontent );
			$SMS_DATA_ENCODING       = $this->setPositionIncrease( 1, $hexcontent );
			$DCS                     = hexdec( $SMS_DATA_ENCODING );
			for ( $currentContentPosition = 1; $currentContentPosition <= 7; $currentContentPosition ++ ) {
				$SMS_TIMESTAMP .= $this->semiOctetToString( $this->setPositionIncrease( 1, $hexcontent ) );
			}
			preg_match_all( "/[0-9]{2}/i", $SMS_TIMESTAMP, $SMS_TIMESTAMP_MATCH );
			$SMS_TIMESTAMP          = $SMS_TIMESTAMP_MATCH[0];
			$SMS_TIMESTAMP_UNIXTIME = strtotime( "20$SMS_TIMESTAMP[0]$SMS_TIMESTAMP[1]$SMS_TIMESTAMP[2] " . ( isset( $SMS_TIMESTAMP[3] ) ? $SMS_TIMESTAMP[3] : "" ) . ":" . ( isset( $SMS_TIMESTAMP[4] ) ? $SMS_TIMESTAMP[4] : "" ) . ":" . ( isset( $SMS_TIMESTAMP[5] ) ? $SMS_TIMESTAMP[5] : "" ) );
			$SMS_DATA_SIZE_HEX      = $this->setPositionIncrease( 1, $hexcontent );
			$SMS_DATA_SIZE          = hexdec( $SMS_DATA_SIZE_HEX );
			$SMS_DATA_PART          = "";
			for ( $currentContentPosition = 1; $currentContentPosition <= $SMS_DATA_SIZE; $currentContentPosition ++ ) {
				$SMS_DATA_PART .= $this->setPositionIncrease( 1, $hexcontent );
			}
			// Fetch the leftovers
			for ( $leftOverIndex = $this->PDU_SMS_POSITION; $leftOverIndex <= count( $hexcontent ); $leftOverIndex ++ ) {
				if ( ! isset( $hexcontent[ $leftOverIndex ] ) ) {
					break;
				}
				$SMS_DATA_PART .= $hexcontent[ $leftOverIndex ];
			}


			/** @var PDULIB_DATA_UDH $SMS_DATA_UDH */
			$SMS_DATA_UDH = $this->getUdh( $SMS_DATA_PART );      // Extract UDH from SMS_DATA_PART

			$byteString = $this->getByteString( $SMS_DATA_PART );
			$this->getPduByteArray( $byteString );

			$SMS_DATA_MESSAGE = $this->getMessageTransformed();

			/** @var PDULIB_DATA_CONTENT $SMS_PDU */
			$SMS_PDU = new PDULIB_DATA_CONTENT( $SMS_CENTRAL, $SMS_DELIVER_BITS, $SMS_TIMESTAMP_UNIXTIME, $SMS_SENDER, $SMS_PROTOCOL_IDENTIFIER, $SMS_DATA_ENCODING, $SMS_DATA_SIZE, $SMS_DATA_PART, $SMS_DATA_UDH, $SMS_DATA_MESSAGE );

			return $SMS_PDU;
		}
	}
}
